package device

import (
	"context"

	"gitlab.com/itbis/iotgw-api/internal/entity"
	"gitlab.com/itbis/iotgw-api/pkg/logger"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access menus from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.Device, error)
	Count(ctx context.Context) (int, error)
	Query(ctx context.Context, offset, limit int) ([]entity.Device, error)
	Create(ctx context.Context, item entity.Device) error
	Update(ctx context.Context, id string, item entity.Device) error
	Delete(ctx context.Context, id string) error
}

// repository persists menus in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.Device, error) {
	var item entity.Device
	err := r.db.Table("device").First(&item, "id=?", id).Error
	return item, err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.Device, error) {
	var menus []entity.Device
	err := r.db.Table("device").Find(&menus).Offset(offset).Limit(limit).Error
	return menus, err
}

// count
func (r repository) Count(ctx context.Context) (int, error) {
	var item entity.Device
	result := r.db.Table("device").Find(&item)
	return int(result.RowsAffected), result.Error
}

// Create saves a new item record in the database.
func (r repository) Create(ctx context.Context, item entity.Device) error {
	return r.db.Table("device").Create(&item).Error
}

// Update ...
func (r repository) Update(ctx context.Context, id string, userNew entity.Device) error {
	var item entity.Device
	err := r.db.Table("device").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}

	err = r.db.Table("device").Where("id = ?", id).Updates(userNew).Error
	return err

}

// Delete ...
func (r repository) Delete(ctx context.Context, id string) error {
	var item entity.Device
	err := r.db.Table("device").First(&item, "id=?", id).Error
	if err != nil {
		return err
	}
	err = r.db.Table("device").Where("id = ?", id).Delete(item).Error
	return err
}
