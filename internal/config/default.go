package config

type Default struct {
	Apps          Apps          `json:"apps"`
	Logger        Logger        `json:"logger"`
	Database      Database      `json:"database"`
	DatabaseNaura DatabaseNaura `json:"databaseNaura"`
}
type Apps struct {
	Name           string `json:"name"`
	Port           string `json:"port"`
	TimeoutContext int    `json:"timeoutContext"`
	JwtSecret      string `json:"jwtSecret"`
	JwtExpired     int    `json:"jwtExpired"`
}
type Logger struct {
	FileTdrLocation    string `json:"fileTdrLocation"`
	FileSyslogLocation string `json:"fileSyslogLocation"`
	FileTdrMaxAge      int    `json:"fileTdrMaxAge"`
	FileSyslogMaxAge   int    `json:"fileSyslogMaxAge"`
	Stdout             bool   `json:"stdout"`
}
type Database struct {
	Host        string `json:"host"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Database    string `json:"database"`
	Port        string `json:"port"`
	Maxidleconn int    `json:"maxIdleConn"`
	Maxopenconn int    `json:"maxOpenConn"`
	Maxlifetime int    `json:"maxLifeTime"`
}

type DatabaseNaura struct {
	Host        string `json:"host"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Database    string `json:"database"`
	Port        string `json:"port"`
	Maxidleconn int    `json:"maxIdleConn"`
	Maxopenconn int    `json:"maxOpenConn"`
	Maxlifetime int    `json:"maxLifeTime"`
}
