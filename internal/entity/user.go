package entity

import (
	"time"
)

// User represents a user.
type User struct {
	ID      int    `json:"id"`
	Name    string `json:"name" validate:"required"`
	Email   string `json:"email" validate:"required"`
	RolesId int    `json:"roles_id"`
	Photo   string `json:"photo"`
	//PartnerId string    `json:"partner_id" validate:"required"`
	Status    string    `json:"status"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	Password  string    `json:"password,omitempty"`
	Token     string    `json:"token,omitempty" gorm:"-"`
}
