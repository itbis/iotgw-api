package entity

// Device
type Device struct {
	ID         int    `json:"id"`
	Name       string `json:"name"`
	OwnerID    int    `json:"ownerId"`
	Imei       string `json:"imei"`
	Token      string `json:"token"`
	DeviceType string `json:"deviceType"`
	IsActive   bool   `json:"isActive"`
}
