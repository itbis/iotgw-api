package auth

import (
	"context"
	"time"

	"gitlab.com/itbis/iotgw-api/internal/entity"
	"gitlab.com/itbis/iotgw-api/middleware"
)

// Usecase ...
type Service interface {
	Login(ctx context.Context, username, password string, middware *middleware.GoMiddleware) (entity.User, error)
	Get(ctx context.Context, username string) (entity.User, error)
}

type service struct {
	repo           Repository
	contextTimeout time.Duration
}

// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
	}
}

// Login ...
func (s service) Login(ctx context.Context, username, password string, middware *middleware.GoMiddleware) (entity.User, error) {
	item, err := s.repo.Login(ctx, username, password)
	if err != nil {
		return item, err
	}

	if item.Email != "" {
		//item.Token = "qazwsx"
		token, _ := middware.CreateToken(item.Name, item.Email, item.RolesId, middware.JwtExpired)
		item.Token = token
	}

	return item, err
}

// Get ...
func (s service) Get(ctx context.Context, username string) (entity.User, error) {
	item, err := s.repo.Get(ctx, username)
	if err != nil {
		return item, err
	}

	return item, err
}
