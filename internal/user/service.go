package user

import (
	"context"
	"time"

	"gitlab.com/itbis/iotgw-api/internal/entity"
)

// Usecase ...
type Service interface {
	Get(ctx context.Context, id string) (entity.User, error)
	Query(ctx context.Context, offset, limit int) ([]entity.User, error)
	Count(ctx context.Context) (int, error)
	Create(ctx context.Context, user entity.User) error
	Update(ctx context.Context, id string, user entity.User) error
	Delete(ctx context.Context, id string) error
}

type service struct {
	repo           Repository
	contextTimeout time.Duration
}

// NewService will create new an service object representation of Service interface
func NewService(repo Repository, timeout time.Duration) Service {
	return service{
		repo:           repo,
		contextTimeout: timeout,
	}
}

// Count ...
func (s service) Count(ctx context.Context) (int, error) {
	return s.repo.Count(ctx)
}

// Get ...
func (s service) Get(ctx context.Context, id string) (entity.User, error) {
	item, err := s.repo.Get(ctx, id)
	if err != nil {
		return item, err
	}
	return item, err
}

// create ...
func (s service) Create(ctx context.Context, user entity.User) error {
	err := s.repo.Create(ctx, user)
	return err
}

// Query ...
func (s service) Query(ctx context.Context, offset, limit int) ([]entity.User, error) {
	items, err := s.repo.Query(ctx, offset, limit)
	if err != nil {
		return items, err
	}

	return items, err
}

// Update ...
func (s service) Update(ctx context.Context, id string, user entity.User) error {
	err := s.repo.Update(ctx, id, user)
	return err
}

// Delete ...
func (s service) Delete(ctx context.Context, id string) error {
	err := s.repo.Delete(ctx, id)
	return err
}
