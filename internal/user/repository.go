package user

import (
	"context"

	"gitlab.com/itbis/iotgw-api/internal/entity"
	"gitlab.com/itbis/iotgw-api/pkg/logger"
	"gitlab.com/itbis/iotgw-api/pkg/util"
	"gorm.io/gorm"
)

// Repository encapsulates the logic to access users from the data source.
type Repository interface {
	Get(ctx context.Context, id string) (entity.User, error)
	Count(ctx context.Context) (int, error)
	Query(ctx context.Context, offset, limit int) ([]entity.User, error)
	Create(ctx context.Context, user entity.User) error
	Update(ctx context.Context, id string, user entity.User) error
	Delete(ctx context.Context, id string) error
}

// repository persists users in database
type repository struct {
	db     *gorm.DB
	logger logger.Logger
}

// NewRepository creates a new repository
func NewRepository(db *gorm.DB, logger logger.Logger) Repository {
	return repository{db, logger}
}

// Get
func (r repository) Get(ctx context.Context, id string) (entity.User, error) {
	var user entity.User
	err := r.db.Table("users").First(&user, "id=?", id).Error
	return user, err
}

// Query
func (r repository) Query(ctx context.Context, offset, limit int) ([]entity.User, error) {
	var users []entity.User
	err := r.db.Table("users").Find(&users).Offset(offset).Limit(limit).Error
	return users, err
}

// count
func (r repository) Count(ctx context.Context) (int, error) {
	var user entity.User
	result := r.db.Table("users").Find(&user)
	return int(result.RowsAffected), result.Error
}

// Create saves a new user record in the database.
func (r repository) Create(ctx context.Context, user entity.User) error {
	user.Password = util.GetMD5Hash(user.Password)
	return r.db.Table("users").Create(&user).Error
}

// Update ...
func (r repository) Update(ctx context.Context, id string, userNew entity.User) error {
	var user entity.User
	err := r.db.Table("users").First(&user, "id=?", id).Error
	if err != nil {
		return err
	}
	if userNew.Password != "" {
		userNew.Password = util.GetMD5Hash(userNew.Password)
	}
	err = r.db.Table("users").Where("id = ?", id).Updates(userNew).Error
	return err

}

// Delete ...
func (r repository) Delete(ctx context.Context, id string) error {
	var user entity.User
	err := r.db.Table("users").First(&user, "id=?", id).Error
	if err != nil {
		return err
	}
	err = r.db.Table("users").Where("id = ?", id).Delete(user).Error
	return err
}
