package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo/v4"
	"github.com/spf13/viper"
	"go.uber.org/zap"

	"gitlab.com/itbis/iotgw-api/internal/auth"
	"gitlab.com/itbis/iotgw-api/internal/config"
	"gitlab.com/itbis/iotgw-api/internal/device"

	"gitlab.com/itbis/iotgw-api/internal/menu"
	"gitlab.com/itbis/iotgw-api/internal/user"
	"gitlab.com/itbis/iotgw-api/middleware"
	"gitlab.com/itbis/iotgw-api/pkg/logger"
)

func main() {

	//setup config
	env := os.Getenv("ENV")
	if env == "" {
		env = "local"
	}

	if len(os.Args) > 1 {
		env = os.Args[1]
	}

	viper := viper.New()
	viper.AddConfigPath("config")
	viper.SetConfigName("config." + env)

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	conf := &config.Default{}
	err = viper.Unmarshal(conf)
	if err != nil {
		panic(fmt.Errorf("unable to decode into struct, %v", err))
	}

	//setup log
	loggerTdr := logger.NewZapLogger(conf.Logger.Stdout, conf.Logger.FileTdrLocation, time.Duration(conf.Logger.FileTdrMaxAge))
	loggerSyslog := logger.NewZapLogger(conf.Logger.Stdout, conf.Logger.FileSyslogLocation, time.Duration(conf.Logger.FileSyslogMaxAge))
	logger := logger.Logger{
		Tdr:    loggerTdr,
		Syslog: loggerSyslog,
	}
	logger.Syslog.Debug("Setup logger done.." + env)

	//setup database
	dsn := conf.Database.Username + ":" + conf.Database.Password + "@tcp(" + conf.Database.Host + ":" + conf.Database.Port + ")/" + conf.Database.Database + "?parseTime=true&loc=Local"
	dbConn, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		logger.Syslog.Error("SetupDb", zap.Any("error", err))
		panic(err)
	}
	logger.Syslog.Debug("Setup db dashboard done..")

	/*dsnNaura := conf.DatabaseNaura.Username + ":" + conf.DatabaseNaura.Password + "@tcp(" + conf.DatabaseNaura.Host + ":" + conf.DatabaseNaura.Port + ")/" + conf.DatabaseNaura.Database + "?parseTime=true&loc=Local"
	dbConnNaura, err := gorm.Open(mysql.Open(dsnNaura), &gorm.Config{})

	if err != nil {
		logger.Syslog.Error("SetupDbNaura", zap.Any("error", err))
		panic(err)
	}
	logger.Syslog.Debug("Setup db naura done..")
	*/

	//setup repo
	timeoutContext := time.Duration(conf.Apps.TimeoutContext) * time.Second
	authRepo := auth.NewRepository(dbConn, logger)
	userRepo := user.NewRepository(dbConn, logger)
	menuRepo := menu.NewRepository(dbConn, logger)
	deviceRepo := device.NewRepository(dbConn, logger)

	//setup use case
	authService := auth.NewService(authRepo, timeoutContext)
	userService := user.NewService(userRepo, timeoutContext)
	menuService := menu.NewService(menuRepo, timeoutContext)
	deviceService := device.NewService(deviceRepo, timeoutContext)

	//register delivery
	e := echo.New()
	middware := middleware.InitMiddleware(conf.Apps.JwtSecret, conf.Apps.JwtExpired)
	e.Use(middware.CORS)
	auth.RegisterHandler(e, authService, logger, middware)
	user.RegisterHandler(e, userService, logger, middware)
	menu.RegisterHandler(e, menuService, logger, middware)
	device.RegisterHandler(e, deviceService, logger, middware)

	// Start server
	go func() {
		if err := e.Start(":" + conf.Apps.Port); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), timeoutContext)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}
